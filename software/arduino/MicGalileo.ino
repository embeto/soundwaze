#include <avr/io.h>

#define ONTIME 30 //on time per column in mili

uint16_t bars[6]={0x0000,0x0000,0x0000,0x0000,0x0000,0x0000};  //each bar holds the values of its leds at a discreate moment of time. There are 12 Leds in a bar, the lowest as physical level located at bit4 and the highest at bit 15.

void setup() 
{
  DDRD=0xFF;
  DDRB|=0b00111111;
  CKSEL&=0b11110000;
}

void refreshLedArray()
/*
 *  sends to the led array display the values from bars variables in the correct form. If it is not called periodically, the display will not be seen as turned on.
 *  parmaeters: None (bars[] array is global declared)
 *  return: void
 */
{
  
  asm("CBI 0x05,3 \n");                                                                  //GroundEN1 equals 0 to turn off group1
  
  asm("CBI 0x05,0 \n");
  PORTD=((bars[0]&0x0F00)<<4)&(bars[1]&0x0F00);                                          //Write corresponding data to DATA BUS
  asm("SBI 0x05,0 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05,0 \n ");                //CLK1 rising as short as possible pulse for Latch1 to copy D[1:8] to Q[1:8]
  asm("CBI 0x05,1 \n"); 
  PORTD=((bars[0]&0x00F0)<<8)&((bars[1]&0x00F0)<<4);                                     //Write corresponding data to DATA BUS
  asm("SBI 0x05,1 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05,1 \n ");                //CLK2 rising as short as possible pulse for Latch2 to copy D[1:8] to Q[1:8]
  asm("CBI 0x05,2 \n");
  PORTD=((bars[0]&0x000F)<<12)&((bars[1]&0x000F)<<8);                                    //Write corresponding data to DATA BUS
  asm("SBI 0x05,2 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05,2 \n ");                //CLK3 rising as short as possible pulse for Latch3 to copy D[1:8] to Q[1:8]
  
  asm("SBI 0x05,3 \n");                                                                  //GroundEN1 equals 1 to turn on group1 after Lathes were set
  delay(ONTIME);                                                                         //delay a well deserved amount of time to light your eyes
  asm("CBI 0x05,3 \n");

  
  asm("CBI 0x05,4 \n");                                                                 //GroundEN2 equals 0 to turn off group2
 
  asm("CBI 0x05,0 \n");       
  PORTD=((bars[2]&0x0F00)<<4)&(bars[3]&0x0F00);                                         //Write corresponding data to DATA BUS                                                      
  asm("SBI 0x05,0 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05,0 \n ");               //CLK1 rising as short as possible pulse for Latch1 to copy D[1:8] to Q[1:8]
  asm("CBI 0x05,1 \n");
  PORTD=((bars[2]&0x00F0)<<8)&((bars[3]&0x00F0)<<4);                                    //Write corresponding data to DATA BUS               
  asm("SBI 0x05,1 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05,1 \n ");               //CLK2 rising as short as possible pulse for Latch2 to copy D[1:8] to Q[1:8]
  asm("CBI 0x05,2 \n");
  PORTD=((bars[2]&0x000F)<<12)&((bars[3]&0x000F)<<8);                                   //Write corresponding data to DATA BUS               
  asm("SBI 0x05,2 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05,2 \n ");               //CLK3 rising as short as possible pulse for Latch3 to copy D[1:8] to Q[1:8]
  
  asm("SBI 0x05,4 \n");                                                                 //GroundEN2 equals 1 to turn on group2 after Lathes were set
  delay(ONTIME);                                                                        //delay a well deserved amount of time to light your eyes
  asm("CBI 0x05,4 \n");
  

  asm("CBI 0x05,5 \n");                                                                 //GroundEN3 equals 0 to turn off group1
  
  asm("CBI 0x05,0 \n");
  PORTD=((bars[3]&0x0F00)<<4)&(bars[4]&0x0F00);                                         //Write corresponding data to DATA BUS 
  asm("SBI 0x05,0 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05,0 \n ");               //CLK1 rising as short as possible pulse for Latch1 to copy D[1:8] to Q[1:8]
  asm("CBI 0x05,1 \n");
  PORTD=((bars[3]&0x00F0)<<8)&((bars[4]&0x00F0)<<4);                                    //Write corresponding data to DATA BUS   
  asm("SBI 0x05,1 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05, 1 \n ");              //CLK2 rising as short as possible pulse for Latch2 to copy D[1:8] to Q[1:8]
  asm("CBI 0x05,2 \n");
  PORTD=((bars[3]&0x00F0)<<8)&((bars[4]&0x00F0)<<4);                                    //Write corresponding data to DATA BUS 
  asm("SBI 0x05,2 \n NOP \n NOP \n NOP \n NOP \n NOP \n CBI 0x05,2 \n ");               //CLK3 rising as short as possible pulse for Latch3 to copy D[1:8] to Q[1:8]
  
  asm("SBI 0x05,5 \n");                                                                 //GroundEN3 equals 1 to turn on group3 after Lathes were set
  delay(ONTIME);                                                                        //delay a well deserved amount of time to light your eyes
  asm("CBI 0x05,5 \n");

     
}
uint8_t C1=0xAA,C2=0xAA,C3=0xAA;
unsigned long int timestamp=millis();
void loop() 
{
  analogRead(A0);

  digitalWrite(11,LOW);

  digitalWrite(8,LOW);
  digitalWrite(9,LOW);
  digitalWrite(10,LOW);
  PORTD=C1;
  digitalWrite(8,HIGH);
  digitalWrite(9,HIGH);
  digitalWrite(10,HIGH);
  delay(1);
  digitalWrite(8,LOW);
  digitalWrite(9,LOW);
  digitalWrite(10,LOW);

  digitalWrite(11,HIGH); 
  delay(3);
  digitalWrite(11,LOW);

  digitalWrite(12,LOW);

  digitalWrite(8,LOW);
  digitalWrite(9,LOW);
  digitalWrite(10,LOW);
  PORTD=C2;
  digitalWrite(8,HIGH);
  digitalWrite(9,HIGH);
  digitalWrite(10,HIGH);
  delay(1);
  digitalWrite(8,LOW);
  digitalWrite(9,LOW);
  digitalWrite(10,LOW);

  digitalWrite(12,HIGH); 
  delay(3);
  digitalWrite(12,LOW);

  digitalWrite(13,LOW);

  digitalWrite(8,LOW);
  digitalWrite(9,LOW);
  digitalWrite(10,LOW);
  PORTD=C3;
  digitalWrite(8,HIGH);
  digitalWrite(9,HIGH);
  digitalWrite(10,HIGH);
  delay(1);
  digitalWrite(8,LOW);
  digitalWrite(9,LOW);
  digitalWrite(10,LOW);

  digitalWrite(13,HIGH); 
  delay(3);
  digitalWrite(13,LOW);
  if(millis()-timestamp>=900)
  {
    timestamp=millis();
    C1=~C1;
    C2=~C2;
    C3=~C3;

    
  }

}