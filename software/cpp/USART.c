/*
***************************************************************************************************************************************
* COPYRIGHT (C) 2016 Razvan Petre
* * Licensed under the Apache License, Version 2.0 (the "License");
* * you may not use this file except in compliance with the License.
* * You may obtain a copy of the License at
* *
* * http://www.apache.org/licenses/LICENSE-2.0
* *
* * Unless required by applicable law or agreed to in writing, software
* * distributed under the License is distributed on an "AS IS" BASIS,
* * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* * See the License for the specific language governing permissions and
* * limitations under the License.

* AUTHOR: Adrian-Razvan Petre
* NAME: USART.c
* PROJECT: Soundwaze (Galileo DCAE Project)
* DESCRIPTION: This module contains USART basic methods for TX/RX via a Serial Communication, implemented from head to tail by Adrian-Razvan Petre using Atmel328P datasheet

***************************************************************************************************************************************
*/
#include "USART.h"
uint16_t values[]={1041,520,259,173,129,86,64,42,32,21,10,9,4};

void USART_init(uint32_t baudrate)
{
	// UMSEL[1:0] is by default 00 which corresponds to Asynchronous USART
    /* Set baud rate */
	switch(baudrate)
	{
	case 2400:
		UBRR0L=values[0];
		UBRR0H=values[0]>>8;
		break;
	case 4800:
		UBRR0L=values[1];
		UBRR0H=values[1]>>8;
		break;
	case 9600:
		UBRR0L=values[2];
		UBRR0H=values[2]>>8;
		break;
	case 14400:
		UBRR0L=values[3];
		UBRR0H=values[3]>>8;
		break;
	case 19200:
		UBRR0L=values[4];
		UBRR0H=values[4]>>8;
		break;
	case 28800:
		UBRR0L=values[5];
		UBRR0H=values[5]>>8;
		break;
	case 38400:
		UBRR0L=values[6];
		UBRR0H=values[6]>>8;
		break;
	case 57600:
		UBRR0L=values[7];
		UBRR0H=values[7]>>8;
		break;
	case 76800:
		UBRR0L=values[8];
		UBRR0H=values[8]>>8;
		break;
	case 115200:
		UBRR0L=values[9];
		UBRR0H=values[9]>>8;
		break;
	case 230400:
		UBRR0L=values[10];
		UBRR0H=values[10]>>8;
		break;
	case 250000:
		UBRR0L=values[11];
		UBRR0H=values[11]>>8;
		break;
	case 500000:
		UBRR0L=values[12];
		UBRR0H=values[12]>>8;
		break;
	default:
		UBRR0L=values[2];
		break;
	}
	/*multiply by 2 UART frequency*/
	UCSR0A|=(1<<U2X0);
    /*Enable receiver and transmitter */
    UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);
    /* Set frame format: 8data, 2stop bit */
    UCSR0C = (1<<USBS0)|(3<<UCSZ00);


}

void USART_TX_byte( uint8_t data)
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) );
	UDR0 = data;
}
void USART_TX_string(char * string, uint8_t endline)
{
	for (uint8_t i=0;i<strlen(string);i++)
		USART_TX_byte(string[i]);
	if(endline)
	{
		USART_TX_byte('\n');
		USART_TX_byte('\r');
	}
}

void USART_TX_number(uint16_t number)
{
	if(number<10)
		USART_TX_byte(number%10+48);
	else
		if(number)
		{
			USART_TX_number(number/10);
			USART_TX_byte(number%10+48);
		}
}

uint8_t USART_RX_byte( void )
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) );
	return UDR0;
}
