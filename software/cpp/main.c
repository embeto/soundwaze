/*
***************************************************************************************************************************************
* COPYRIGHT (C) 2016 Razvan Petre
* * Licensed under the Apache License, Version 2.0 (the "License");
* * you may not use this file except in compliance with the License.
* * You may obtain a copy of the License at
* *
* * http://www.apache.org/licenses/LICENSE-2.0
* *
* * Unless required by applicable law or agreed to in writing, software
* * distributed under the License is distributed on an "AS IS" BASIS,
* * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* * See the License for the specific language governing permissions and
* * limitations under the License.

* AUTHOR: Adrian-Razvan Petre
* NAME: main.c
* PROJECT: Soundwaze (Galileo DCAE Project)
* DESCRIPTION: This module contains main function for the Soundwaze system and adjacent methods and ISRs() 

***************************************************************************************************************************************
*/

#define F_CPU 20000000UL										//must be before including libraries because these uses the F_CPU define

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <math.h>

#include "USART.h"
#include "GlobalPara.h"

//each bar holds the values of its LEDs at a discrete moment of time.
//There are 12 LEDs in a bar, the lowest LED state is stored at bit0 and the highest at bit 11.
//Even if from hardware considerations a logical 0 will turn on the led, the algorithm is designed using positive logics
//Before sending the data to the DATA[7:0] bus, it is inverted by the method written

//*****DISPLAY Data Structures******
uint16_t  bars[]={0x0AAA,0x0AAA,0x0AAA,0x0AAA,0x0AAA,0x0AAA};
uint16_t thermometer_values[]={0x000,0x001,0x003,0x007,0x00F,0x01F,0x03F,0x07F,0x0FF,0x1FF,0x3FF,0x7FF,0xFFF};
//*****ADC Data Structures******
uint8_t rawPkPk_flag=0, polling_counter=0;
uint16_t rawPkPk=0,cy=0x00,offset=0x00;
//*****so called FSM Machine******
uint8_t machine_state;


void setup()
{
/*
 * method runs only once at the beginning of the program
 * setup all the needed parameters
 * return: void
 */
	cli();								//enable global interrupts
	USART_init(230400);					//init UART (8data, no parity, 1stop bit by default and Enable USART RX and TX)
	sei();
	TCCR1B|=1<<CS12|1<<CS10|1<<WGM12;	//fCLK/1024-->fTIM=19k53 --> TTIM=51.20us
	//OCR1A=0x1312;						//sampleWindows=250ms --> OCR1A=250000/51.2=4882|dec=1312|hex
	//OCR1A=0x07A1;						//sampleWindows=100ms --> OCR1A=100000/51.2=1953|dec=07A1|hex
	OCR1A=0x00C3;						//sampleWindows=10ms --> OCR1A=10000/51.2=195|dec=00C3|hex
	TIMSK1|=(1<<OCIE1A);

	/*ADC Initialization*/
	ADMUX|=0x02;						//select from the ADMUX the 2nd pin
	ADCSRA|=(1<<ADEN);
	ADCSRA|=0x07;						//fCLK_ADC=fSYS/128 (is okay, do not overcome the 200Khz maxim limit from DATASHEET)
	ADMUX|=(1<<ADLAR);					//Left ADJUST (only 8 bits are enough)
	ADCSRA|=(1<<ADIE);					//enable interrupt
	ADMUX|=(1<<REFS0);					//reference in AVCC (ADC Power Source)
	ADCSRA|=(1<<ADSC);					//start fist conversion


	/*GPIO Pins selection*/
	DDRD|=0xFF;
	DDRC|=0x03;
	DDRB|=0b00111111;
	PORTB|=0b00000111;

}

void refreshLedArray()
{

	/*
	 *  sends to the led array display the values from bars variables in the correct form.
	 *  If it is not called periodically, the display will NOT appear as turned on.
	 *  parameters: None (bars[] array is global declared)
	 *  return: void
	 */
	uint8_t data;
	PORTB&=~(1<<GEN1);


		data=~(((bars[0]&0x000F)|((uint16_t)(bars[1]&0x000F)<<4)));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK3);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK3);

		data=~(((bars[0]&0x00F0)>>4)|(bars[1]&0x00F0));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK2);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK2);

		data=~(((bars[0]&0x0F00)>>8)|((bars[1]&0x0F00)>>4));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK1);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK1);//	switch (machine_state)

	PORTB|=(1<<GEN1);
	_delay_ms(ONTIME);
	PORTB&=~(1<<GEN1);

	PORTB&=~(1<<GEN2);

		data=~((bars[2]&0x000F)|((uint16_t)(bars[3]&0x000F)<<4));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK3);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK3);

		data=~(((bars[2]&0x00F0)>>4)|(bars[3]&0x00F0));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK2);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK2);

		data=~(((bars[2]&0x0F00)>>8)|((bars[3]&0x0F00)>>4));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK1);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK1);

	PORTB|=(1<<GEN2);
	_delay_ms(ONTIME);
	PORTB&=~(1<<GEN2);

	PORTB&=~(1<<GEN3);

		data=~((bars[4]&0x000F)|((uint16_t)(bars[5]&0x000F)<<4));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK3);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK3);

		data=~(((bars[4]&0x00F0)>>4)|(bars[5]&0x00F0));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK2);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK2);

		data=~(((bars[4]&0x0F00)>>8)|((bars[5]&0x0F00)>>4));
		PORTD&=0x03;
		PORTD|=data&0xFC;
		PORTC&=0xFC;
		PORTC|=data&0x03;
		_delay_us(1);
		PORTB&=~(1<<CLK1);
		_delay_us(CLKTIME);
		PORTB|=(1<<CLK1);

	PORTB|=(1<<GEN3);
	_delay_ms(ONTIME);
	PORTB&=~(1<<GEN3);

}
void fillAllBars(uint8_t index)
/*
 * Fills up all the 6 bars with the corresponding value from the thermometer_values array according to the index given
 * To create a nice view the extremities are filled with a smaller value to give the impression on a histogram
 * return: void
 */
{
	bars[0]=thermometer_values[(index-2)>0?(index-2):(index-1)>0?(index-1):index];
	bars[1]=thermometer_values[(index-1)>0?(index-1):index];
	bars[2]=thermometer_values[index];
	bars[3]=thermometer_values[index];
	bars[4]=thermometer_values[(index-1)>0?(index-1):index];
	bars[5]=thermometer_values[(index-2)>0?(index-2):(index-1)>0?(index-1):index];
}

void refreshBarsDS(void)
/*
 * refresh the bars Data Structure according to rawPkPk value
 * the refresh is made according to the thresholds constant array in a thermometrical way
 * return: void
 */
{
	if(rawPkPk<offset)
		fillAllBars(0);
	for (uint8_t i=0;i<12;i++)
		if((rawPkPk>(offset+cy*i))&&(rawPkPk<(offset+cy*(i+1))))
		{
			fillAllBars(i+1);
			break;
		}

}
int main(void)
{
	setup();
	while (1)
	{
		if(rawPkPk_flag==1)
		{
			//debugging only

			USART_TX_string("MIC= ",0);
			USART_TX_number(rawPkPk);
			USART_TX_string(" cy= ",0);
			USART_TX_number(cy);
			USART_TX_string(" offset= ",0);
			USART_TX_number(offset);
			USART_TX_string("",1);

			refreshBarsDS();
			rawPkPk_flag=0;
		}
		refreshLedArray();
	}

}



ISR(TIMER1_COMPA_vect)
/*
 * TIMER1_COMPA_vect implements a so called FSF (Finite State Machine) using machine_state uint8_t variable
 * T_TIMER1_COMPA_vect=10ms set by OCR1A at the beginning in setup()
 * every 10*10=100ms a new ADC from MIC is performed
 * every 100*10=1000ms a new ADC from potentiometer 1 and potentiometer 2 is performed
 * each conversion process is continuous: The ADC ISR gives itself a new START CONV until this ISR (FSM) will disable the ADC
 * each conversion takes around 10ms and 90ms the FST is in 'default' state just increasing its own state (the display is let to update via main)
 * the fADC=20 000kHz/128=156kHz --> TCONV=13.5*TADC=86us. This means that around 10 000us/86us=116 Conversions will be made in a windows of 10ms
 * IN THIS LATER IMPLEMENTATION Figure12 from soundwaze.pdf system description is OUTDATED and will be updated ASAP!!
 */
{

	switch(machine_state)
	{
	case 0:
	case 10:
	case 20:
	case 30:
	case 40:
	case 50:
	case 60:
	case 70:
	case 80:
	case 90:
		rawPkPk=0;
		polling_counter=0;
		ADCSRA&=~(1<<ADEN);
		ADMUX&=0xF0;
		ADMUX|=0x02;
		ADCSRA|=(1<<ADEN);
		ADCSRA|=(1<<ADSC);
		machine_state++;
		break;
	case 1:
	case 11:
	case 21:
	case 31:
	case 41:
	case 51:
	case 61:
	case 71:
	case 81:
	case 91:
		rawPkPk_flag=1;
		rawPkPk=rawPkPk/(polling_counter);
		ADCSRA&=~(1<<ADEN);
		machine_state++;
		break;
	case 97:
		offset=0;
		polling_counter=0;
		ADCSRA&=~(1<<ADEN);
		ADMUX&=0xF0;
		ADMUX|=0x03;
		ADCSRA|=(1<<ADEN);
		ADCSRA|=(1<<ADSC);
		machine_state++;
		break;
	case 98:
		cy=0;
		polling_counter=0;
		ADCSRA&=~(1<<ADEN);
		ADMUX&=0xF0;
		ADMUX|=0x04;
		ADCSRA|=(1<<ADEN);
		ADCSRA|=(1<<ADSC);
		machine_state++;
		break;
	case 99:
		machine_state=0;
		break;
	default:
		machine_state++;
		break;
	}
}

ISR(USART_RX_vect)
/*
 * debugging only, in case some string/number is send from PC
 */
{

}

ISR(ADC_vect)
{
	int16_t raw=ADCH;

	switch (ADMUX&0x0F)
	{
	case 0x02:						//reading MIC info from PC2
		if(raw>128)			//is is saved the x[n]^2 value according to Discrete Signal Theory. Later in FSM is calculated the Power of this signal
			rawPkPk+=(raw-128)*(raw-128);
		else
			rawPkPk+=(128-raw)*(128-raw);
		polling_counter++;
		ADCSRA|=(1<<ADSC);
		break;
	case 0x03:						//reading potentiometer1 from PC3
		offset=(raw>>1);
		break;
	case 0x04:						//reading potentiometer2 from PC4
		cy=(raw>>2)+4;
		break;
	}

}

