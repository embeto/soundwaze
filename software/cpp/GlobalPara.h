/*
***************************************************************************************************************************************
* COPYRIGHT (C) 2016 Razvan Petre
* * Licensed under the Apache License, Version 2.0 (the "License");
* * you may not use this file except in compliance with the License.
* * You may obtain a copy of the License at
* *
* * http://www.apache.org/licenses/LICENSE-2.0
* *
* * Unless required by applicable law or agreed to in writing, software
* * distributed under the License is distributed on an "AS IS" BASIS,
* * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* * See the License for the specific language governing permissions and
* * limitations under the License.

* AUTHOR: Adrian-Razvan Petre 
* NAME: GlobalPara.h
* PROJECT: Soundwaze (Galileo DCAE Project)
* DESCRIPTION: This module contains global parameters for the Soundwaze system, hardware connection and so on

***************************************************************************************************************************************
*/

#ifndef GLOBALPARA_H_
#define GLOBALPARA_H_

#define ONTIME 4 												//on time per column in milliseconds
#define CLKTIME 5												//clock on time in us


#define TIMSK1 _SFR_MEM8(0x6F)

#define CLK1 0
#define CLK2 1
#define CLK3 2
#define GEN1 3
#define GEN2 4
#define GEN3 5

#endif /* GLOBALPARA_H_ */
