***************************************************************************************************************************************
* COPYRIGHT(C) 2016 Razvan Petre
* * Licensed under the Apache License, Version 2.0 (the "License");
* * you may not use this file except in compliance with the License.
* * You may obtain a copy of the License at
* *
* * http://www.apache.org/licenses/LICENSE-2.0
* *
* * Unless required by applicable law or agreed to in writing, software
* * distributed under the License is distributed on an "AS IS" BASIS,
* * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* * See the License for the specific language governing permissions and
* * limitations under the License.

* AUTHOR: Adrian - Razvan Petre
* NAME : USART.h
* PROJECT : Soundwaze(Galileo DCAE Project)
* DESCRIPTION : This module contains USART header files and methods description for Serial Communication

***************************************************************************************************************************************
* /


#ifndef USART_H_
#define USART_H_

#ifndef io_h
#include <avr/io.h>
#endif
#ifndef string_h
#include <string.h>
#endif



void USART_init(uint32_t baudrate);
/*
 * initialize all the USART communication
 * it is hard-coded in the method that it is a Asynchronous USART
 * takes 1 parameter: baudrate in the following space: {2400,4800,9600,14400,19200,28800,38400,57600,76800,115200,230400,250000,500000};
 * if parameter is not in the space sets by default 9600 baudrate
 * it is also hard-coded the 8bits and 2stop bits configuration
 * return: void
 */
void USART_TX_byte(uint8_t data);
/*
 * transmits a byte taken as parameter as soon as UART TX is free
 * return: void
 */
void USART_TX_string(char * string, uint8_t endline);
/*
 * transmit a string taken as parameter
 * endline: if it is 1 a new line and carriage return are added at the end
 * return: void
 */
void USART_TX_number(uint16_t number);
/*
 * transmit a number given as parameter
 * does not send neither carriage return nor line feed at the end
 * return: void
 */

uint8_t USART_RX_byte(void);
/*
 * return: a byte from the RX buffer as soon as it is available
 */

#endif /* USART_H_ */
