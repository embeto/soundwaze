# soundwaze - Sound intensity level meter #
* https://embeto.com/soundwaze/
* The sound intensity from the environment is visible on a 12*6 LEDs display 
* calibration option with two potentiometers
* with a 2.2Amps rechargable battery it gives up to 5 hours of continous use
* Detailed focus on a sleek design embedded in a closed transparent case

## Repository Structure ##

* documents 
	* datasheets 
	* schematics - each tapeout version is listed here in a directory containing the corresponding BOM and PDF schematic. Blocks directory has the corresponding tapeout block schematic and the containing blocks which were not phisically produced as seen.
	* documentation - resources and product description. Also presentation included here
	* sound_test - screenshots from sound proudction app used to calibrate microphone
	* workflow - txt file with work hours on this project	

* pcbDesign 
	* AnalogFTB_block - (concept) test design implementation with analog filters - dropped and opted for a digital filtering option
	* DigitalFTB_block - (concept) design implementation with digital filters, full blocks schematic of the system here
	* LEDs_module
		* schematics - Working directory for LEDs module circuit board
		* tapeouts (see *circuit Board design* section for more)
	* MCU_module
		* schematics - Working directory for MCU module
		* tapeouts (see *circuit Board design* section for more) - every PCB tapeout is listed here in a directory containing the exact schematic and gerber files

* software 
	* arduino
	* cpp - eclipse project directory - software designed in eclipse and written in C
	* sh - linux scripts 
	
## Circuit Board design ##

* LEDs_module
	* v1.0 - (in-house produced) LEDs circuit board implemented as LEDs block from main but with connectors added

* MCU_module
	* v1.0 - (in-house produced) MCU circuit board implemented as MCU block from main but with connectors added

## Documentation Links ##
* [Audio Filter explained](http://www.teachmeaudio.com/mixing/techniques/audio-spectrum)
* [Arduino music visualisation](https://learn.adafruit.com/piccolo/code)
* [Audio specter monitor lib](http://elm-chan.org/works/akilcd/report_e.html)
* [Arduino music visualizer GitHub](https://github.com/adafruit/piccolo)